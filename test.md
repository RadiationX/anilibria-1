```
proxy_cache_path /var/www/test use_temp_path=off levels=1:2 keys_zone=test:125m inactive=30d max_size=1000g;
proxy_cache_lock on;
proxy_cache_min_uses 1;
proxy_cache_revalidate on;
```

<img src="https://img.poiuty.com/img/47/93fab86511fe45d34c6646b89b7cd847.png">

Cache size 420GB => AVG hit 87%.<br/>
Cache size 640GB => AVG hit 95%.<br/>
+8% стоит 220GB disk space. Размер кэша продолжает расти (726G). Пратически не увеличивает hit rate.<br/>

<img src="https://img.poiuty.com/img/13/ce0342298a091b5cde9c67dc8dba2613.png">

```
#!/bin/bash

echo -n "Total files: "
find /var/www/test -type f | wc -l
for i in `seq 360 360 1800`; do 
    x=$(($i - 360))
    echo -n "Files accessed between $x and $i minutes ago: "
    find /var/www/test -type f -amin +$x -amin -$i | wc -l
done
```

Думаю стоит увеличить `proxy_cache_min_uses 3`, уменьшить `inactive=1d`. Посмотреть `cache size`.

```
Total files: 540698
Files accessed between than 0 and 360 minutes ago:        96244  =>  17.8 %
Files accessed between than 360 and 720 minutes ago:     161413  =>  29.8 %
Files accessed between than 720 and 1080 minutes ago:    161516  =>  29.9 %
Files accessed between than 1080 and 1440 minutes ago:   119405  =>  22.1 %
Files accessed between than 1440 and 1800 minutes ago:     2141  =>   0.4 %
```

<img src="https://img.poiuty.com/img/11/c0159446ccbbbe34c6fff63978d8cf11.png">

```
# iostat -xk -t 300
02/03/2018 06:46:33 AM
avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           1.21    0.07    2.32   16.17    0.00   80.24

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
nvme0n1           0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
sdb               8.25    34.18   78.58   22.37 17601.51  4778.61   443.39     1.94   19.22   10.55   49.67   4.86  49.09
sdc               8.80    32.32  106.63   20.58 19894.36  4740.21   387.32     1.69   13.31    8.21   39.71   4.33  55.11
sdd               8.87    32.32   79.00   20.58 17246.31  4740.20   441.60     1.59   15.93   10.73   35.87   4.88  48.62
sda               8.17    34.14  105.69   22.40 19810.13  4778.61   383.91     2.02   15.75    8.61   49.42   4.36  55.82
md1               0.00     0.00  405.98   80.05 113216.56  9597.15   505.38     0.00    0.00    0.00    0.00   0.00   0.00
md0               0.00     0.00    0.26    5.19     3.37    48.16    18.91     0.00    0.00    0.00    0.00   0.00   0.00
```

Дополнительную нагрузку создает <a href="https://github.com/NebulousLabs/Sia">siad</a>. Выключаем - падает.

<img src="https://img.poiuty.com/img/38/e7c173d6414fd0aeacae21a13b449a38.png">

<img src="https://img.poiuty.com/img/19/b00ac301e47d3de5f709e289019e5719.png">

ZFS vs EXT4. Было меньше RAM (16GB).

<img src="https://img.poiuty.com/img/3c/7f5386f2c01de5fe24eaa1970c9b7f3c.png">

<hr/>

`proxy_cache_lock` если включено, одновременно только одному запросу будет позволено заполнить новый элемент кэша, идентифицируемый согласно директиве `proxy_cache_key`, передав запрос на проксируемый сервер. Остальные запросы этого же элемента будут либо ожидать появления ответа в кэше, либо освобождения блокировки этого элемента, в течение времени, заданного директивой `proxy_cache_lock_timeout`.

```
proxy_cache_lock on;
proxy_cache_min_uses 3;
```

Делает три запроса на сервер. Последний - записывает в кэш.
```
# siege -t60S http://192.168.0.92/test.ts
** Preparing 25 concurrent users for battle

192.168.0.92 - - [03/Feb/2018:10:52:26 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:52:26 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:52:26 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
```

Выключим. Двадцать пять запросов.
```
proxy_cache_lock off;
proxy_cache_min_uses 3;
```

```
# siege -t60S http://192.168.0.92/test.ts
** Preparing 25 concurrent users for battle

192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
192.168.0.92 - - [03/Feb/2018:10:56:32 -0500] "GET /test.ts HTTP/1.0" 200 5242880 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.2"
```
<hr/>

Несколько `proxy_cache_path`.
```
proxy_cache_path /var/www/cache use_temp_path=off levels=1:2 keys_zone=cache:125m inactive=1d max_size=450g;
proxy_cache_path /var/www/test use_temp_path=off levels=1:2 keys_zone=test:125m inactive=1d max_size=1000g;

split_clients $request_uri $cachedisk {
	50% "cache";
	50% "test";
}

server {
	...
	proxy_cache    $cachedisk;
	...
```

<hr/>

`Keepalive connections` can have a major impact on performance by reducing the CPU and network overhead needed to open and close connections. NGINX terminates all client connections and creates separate and independent connections to the upstream servers. NGINX supports keepalives for both clients and upstream servers.

```
# netstat -tupan | grep 5.9.82.141
tcp        0      0 109.248.206.13:34834    5.9.82.141:80           ESTABLISHED 15062/nginx: worker 
tcp        0      0 109.248.206.13:34838    5.9.82.141:80           ESTABLISHED 15059/nginx: worker 
tcp        0    564 109.248.206.13:34842    5.9.82.141:80           ESTABLISHED 15058/nginx: worker 
tcp        0      0 109.248.206.13:34826    5.9.82.141:80           ESTABLISHED 15063/nginx: worker 
tcp        0      0 109.248.206.13:34830    5.9.82.141:80           ESTABLISHED 15058/nginx: worker 
tcp        0      0 109.248.206.13:34828    5.9.82.141:80           ESTABLISHED 15057/nginx: worker 
tcp        0      1 109.248.206.13:34844    5.9.82.141:80           SYN_SENT    15059/nginx: worker 
tcp        0      0 109.248.206.13:34786    5.9.82.141:80           ESTABLISHED 15061/nginx: worker 
tcp        0      0 109.248.206.13:34840    5.9.82.141:80           ESTABLISHED 15061/nginx: worker 
tcp        0      0 109.248.206.13:34764    5.9.82.141:80           ESTABLISHED 15061/nginx: worker 
```

Включаем.
```
proxy_http_version 1.1;
proxy_set_header Connection "";
```
```
# netstat -tupan | grep 5.9.82.141
tcp        0      0 109.248.206.13:36958    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:36930    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37156    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37412    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37458    5.9.82.141:80           ESTABLISHED 32043/nginx: worker 
tcp        0      0 109.248.206.13:37250    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37510    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37076    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37252    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37316    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37494    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37176    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37474    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37448    5.9.82.141:80           TIME_WAIT   -                   
tcp        0      0 109.248.206.13:37578    5.9.82.141:80           ESTABLISHED 32042/nginx: worker 
```

<hr/>

Каждый новый запроc nginx записывает в `access.log` => много мелких операций.

```
request <==> nginx <==> write log
request <==> nginx <==> write log
request <==> nginx <==> write log
```

Выгоднее записать данные в `buffer`. Потом, за одну операцию, сохранить в `access.log`.<br/>
Если установить `flush=5s`, то запись в файл будет происходить каждые пять секунд или когда `buffer > size`.

```
access_log /var/log/nginx/cache-access.log cache buffer=16k;
```

```
request <==> nginx <==> write buffer
request <==> nginx <==> write buffer
request <==> nginx <==> write buffer

========= if buffer > size =========
buffer ==> write log ==> clean buffer
```

Если вам не нужны логи/ статистика - отключите `access.log`.
```
access_log off;
```
